//
// Created by jozef on 10. 11. 2020.
//
#include "Menu.h"

#define MAX_ITEMS 6
#define SPACE_COUNT 65
#define MAX_HEADER 8

Menu::Menu(const std::string& header, const std::vector<std::string>& items) {
    if (header.length() < MAX_HEADER) {
        if (items.size() <= MAX_ITEMS) {
            this->items = items;
            this->head = header;
            this->active = 0;
            this->map = std::vector<bool>({true, true, true, true, true, true});
            //Keyboard::getKeyboard()->addListener(*this);
            Keyboard::getKeyboard()->addListener(this);
        } else throw std::out_of_range("Menu::Menu - items too long\n");
    } else throw std::out_of_range("Menu::Menu - header too long\n");
}

Menu::~Menu() {
}

bool Menu::addNewItem(const std::string &item) {
    if (this->items.size() < MAX_ITEMS) {
        this->items.push_back(item);
        return true;
    }
    return false;
}

bool Menu::addNewItems(const std::vector<std::string> &items) {
    if (this->items.size() + items.size() <= MAX_ITEMS) {
        this->items.insert(this->items.end(), items.begin(), items.end());
        return true;
    }
    return false;
}

bool Menu::show() const {
    std::string menu;
    this->printSpace(menu); // vypise medzery aby nebolo vidiet predchadzajuce menu
    menu.append("=============================================================================== \n"); // zaciatok
    this->printHeader(menu); // hlavicka
    this->printButtons(menu); // tlacidla
    menu.append("=============================================================================== \n"); // koniec
    this->printMessage(menu);
    fwrite(menu.c_str(), sizeof(char), menu.length(), stdout);
    return false;
}

bool Menu::change(const unsigned short &position, const std::string &item) {
    if (position < this->items.size() && position >= 0) {
        this->items[position] = item;
        return true;
    }
    return false;
}

bool Menu::remove(const unsigned short &position) {
    if (position - 1 < this->items.size() && position > 0) {
        this->items.erase(this->items.begin() + position);
        if (this->active == position - 1) {
            this->active = 0;
        }
        return true;
    }
    return false;
}

bool Menu::replace(const std::vector<std::string> items) {
    if (items.size() <= MAX_ITEMS) {
        this->items.clear();
        this->items.insert(this->items.end(), items.begin(), items.end());
        return true;
    }
    return false;
}

void Menu::setActiveMap(const std::vector<bool> &map) {
    this->map = map;
}

void Menu::printSpace(std::string &menu) const {
    for (int i = 0; i < SPACE_COUNT; ++i) {
        menu.append("\n");
    }
}

bool Menu::header(const std::string &header) {
    this->head = header;
    return false;
}

void Menu::printHeader(std::string &menu) const {
    unsigned short corr = (79 - this->head.length() * 9) % 2;
    unsigned short space = (79 - this->head.length() * 9) / 2;
    for (int j = 0; j < 5; ++j) {
        menu.append("|");
        for (int a = 0; a < space; ++a) {
            menu.append(" "); // medzery
        }
        for (int i = 0; i < this->head.length(); ++i) { // pismena
            if (this->head[i] > 90) {
                menu.append(letters[this->head[i] - 'a'][j]);
            } else {
                menu.append(letters[this->head[i] - 'A'][j]);
            }
            menu.append("  ");
        }
        for (int a = 0; a < space - 2; ++a) {
            menu.append(" "); // medzery
        }
        if (corr == 1) {
            menu.append(" ");
        }
        menu.append("|\n");
    }
}

void Menu::printButtons(std::string &menu) const {
    if (this->items.size() > 3) { // 2 stlpce - tj ak ma viac ako 3 polozky
        for (int i = 0; i < 3; ++i) { // vzdy vypise 3 riadky
            // triedenie ze ci je zobrazena prava, lava alebo ziadna moznost
            if (i == this->active) {
                menu.append("|      ______________________________                                         |\n");
                menu.append("|     |                              |                                        |\n");
                menu.append("|     |");
            } else if (i + 3 == this->active) {
                menu.append("|                                         ______________________________      |\n");
                menu.append("|                                        |                              |     |\n");
                menu.append("|      ");
            } else {
                menu.append("|                                                                             |\n");
                menu.append("|                                                                             |\n");
                menu.append("|      ");
            }

            // ak existuje lava  strana
            if (i < this->items.size()) {
                unsigned short space = (30 - this->items[i].length()) / 2;
                unsigned short corr = (30 - this->items[i].length()) % 2;
                for (int j = 0; j < space; ++j) {
                    menu.append(" ");
                }
                menu.append(this->items[i]);
                for (int j = 0; j < space; ++j) {
                    menu.append(" ");
                }
                if (corr > 0) {
                    menu.append(" ");
                }
            } else {
                menu.append("                              ");
            }

            // opat triedenie ci je niektora cast oznacena
            if (i == this->active) {
                menu.append("|    ");
            } else if (i + 3 == this->active) {
                menu.append("    |");
            } else {
                menu.append("     ");
            }

            // ak existuje prava strana
            if (i + 3 < this->items.size()) {
                unsigned short space = (30 - this->items[i + 3].length()) / 2;
                unsigned short corr = (30 - this->items[i + 3].length()) % 2;
                for (int j = 0; j < space; ++j) {
                    menu.append(" ");
                }
                menu.append(this->items[i + 3]);
                for (int j = 0; j < space; ++j) {
                    menu.append(" ");
                }
                if (corr > 0) {
                    menu.append(" ");
                }
            } else {
                menu.append("                              ");
            }

            // a zase triedenie
            if (i == this->active) {
                menu.append("      |\n");
                menu.append("|     |______________________________|                                        |\n");
            } else if (i + 3 == this->active) {
                menu.append("|     |\n");
                menu.append("|                                        |______________________________|     |\n");
            } else {
                menu.append("      |\n");
                menu.append("|                                                                             |\n");
            }
        }
    } else { // 1 stlpec - mozu byt dlhsie texty
        for (int i = 0; i < 3; ++i) {
            unsigned short space = 25;
            unsigned short corr = 1;
            if (i < this->items.size()) {
                space = (51 - this->items[i].length()) / 2;
                corr = (51 - this->items[i].length()) % 2;
            }
            if (i == this->active) {
                menu.append("|             ___________________________________________________             |\n");
                menu.append("|            |                                                   |            |\n");
                menu.append("|            |");
            } else {
                menu.append("|                                                                             |\n");
                menu.append("|                                                                             |\n");
                menu.append("|             ");
            }
            for (int j = 0; j < space; ++j) {
                menu.append(" ");
            }
            if (i < this->items.size()) {
                menu.append(this->items[i]);
            }
            for (int j = 0; j < space; ++j) {
                menu.append(" ");
            }
            if (corr > 0) {
                menu.append(" ");
            }
            if (i == this->active) {
                menu.append("|            |\n");
                menu.append("|            |___________________________________________________|            |\n");
            } else {
                menu.append("             |\n");
                menu.append("|                                                                             |\n");
            }
        }
    }
}

void Menu::printMessage(std::string &menu) const {
    menu.append("|                                                                             | \n");
    menu.append("|");
    unsigned short space = (77 - this->message.length()) / 2;
    unsigned short corr = (77 - this->message.length()) % 2;
    for (int i = 0; i < space; ++i) {
        menu.append(" ");
    }
    menu.append(this->message);
    for (int i = 0; i < space; ++i) {
        menu.append(" ");
    }
    if (corr > 0) {
        menu.append(" ");
    }
    menu.append("| \n");
    menu.append("|                                                                             | \n");
    menu.append("=============================================================================== \n");
}

void Menu::setMessage(const std::string &message) {
    this->message = message;
}

void Menu::onKeyPress(char key) {
    // 119 W // 65
    // 97 A // 68
    // 115 S // 66
    // 100 D // 67
    if (key == 119 || key == 65) {
        do {
            --this->active;
            if (this->active == -1) {
                this->active = this->items.size() - 1;
            }
        } while (!this->map[this->active]);
        this->show();
    } else if (key == 115 || key == 66) {
        do {
            ++this->active;
            if (this->active == this->items.size()) {
                this->active = 0;
            }
        } while (!this->map[this->active]);
        this->show();
    } else if (key == 97 || key == 68) {
        do {
            if (this->active < 3) {
                if (this->active == 0) {
                    this->active = 4;
                }
                this->active += 2;
                if (this->active >= this->items.size()) {
                    this->active = this->items.size() - 1;
                }
            } else {
                this->active -= 3;
                if (this->active < 0) {
                    this->active = 0;
                }
            }
        } while (!this->map[this->active]);
        this->show();
    } else if (key == 100 || key == 67) {
        do {
            if (this->active < 3) {
                this->active += 3;
                if (this->active >= this->items.size()) {
                    this->active = this->items.size() - 1;
                }
            } else {
                if (this->active == 5) {
                    this->active = 2;
                }
                this->active -= 2;
                if (this->active < 0) {
                    this->active = 0;
                }
            }
        } while (!this->map[this->active]);
        this->show();
    }

    //int k = key;
    //std::cout<<k<<"\n";
    if (key == 10) {
        onMenuEnter(this->active);
    }
    onKeyPressCall(key);
}

void Menu::setActive(int pos) {
    this->active = pos;
    this->show();
}

#undef MAX_HEADER
#undef SPACE_COUNT
#undef MAX_ITEMS