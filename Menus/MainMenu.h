//
// Created by jozef on 26. 12. 2020.
//

#ifndef SEMESTRALNAPRACA_MAINMENU_H
#define SEMESTRALNAPRACA_MAINMENU_H

#include "Menu.h"

class MainMenu : public Menu {
public:
    MainMenu();
    ~MainMenu() override;
    void onKeyPressCall(char key) override;
    void onMenuEnter(int choice) override;
    void onKeyPress(char key) override;
private:
};


#endif //SEMESTRALNAPRACA_MAINMENU_H
