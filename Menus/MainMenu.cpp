//
// Created by jozef on 26. 12. 2020.
//

#include "MainMenu.h"
#include "../Game.h"
#include "MenuStart.h"
#include "MenuScore.h"

MainMenu::MainMenu() :
    Menu::Menu("Boolan", std::vector<std::string>({"Spustit hru", "Zobrazit skore", "Koniec hry"})) {

    this->setMessage("Pomocou W,A,S,D sa pohybujete v menu. ENTER vyberie zvolenu moznost.");
}

MainMenu::~MainMenu() {
}


void MainMenu::onKeyPressCall(char key) {
    //std::cout<<"Key: " << key << "\n";
}

enum class CHOICES_MAINMENU {
    START = 0,
    SCORE = 1,
    END = 2
};

void MainMenu::onMenuEnter(int choice) {
    auto ch = (CHOICES_MAINMENU)(choice);
    if (ch == CHOICES_MAINMENU::START) {
        Game::getGame()->setMenu(new MenuStart());
    } else if (ch == CHOICES_MAINMENU::SCORE) {
        Game::getGame()->setMenu(new MenuScore());
    }else if (ch == CHOICES_MAINMENU::END) {
        Keyboard::getKeyboard()->stop();
    }
}

void MainMenu::onKeyPress(char key) {
    Menu::onKeyPress(key);
}
