//
// Created by jozef on 6. 1. 2021.
//

#include "MenuFinal.h"
#include "../Game.h"
#include "MainMenu.h"

MenuFinal::MenuFinal() :
    Menu::Menu("Final", std::vector<std::string>({"...", "...", "...", "...", "", "Hlavne menu"})) {

    Menu::setActiveMap(std::vector<bool>({false, false, false, false, false, true}));
    Menu::setActive(5);
}

MenuFinal::~MenuFinal() {
    Game::getGame()->stopConnect(true);
    Game::getGame()->stopServer();
}

void MenuFinal::onKeyPressCall(char key) {
}

void MenuFinal::onMenuEnter(int choice) {
    Game::getGame()->setMenu(new MainMenu());
}
