//
// Created by jozef on 26. 12. 2020.
//

#ifndef SEMESTRALNAPRACA_MENUSTART_H
#define SEMESTRALNAPRACA_MENUSTART_H

#include "Menu.h"

class MenuStart : public Menu {
public:
    MenuStart();
    ~MenuStart() override;
    void onKeyPressCall(char key) override;
    void onMenuEnter(int choice) override;
    std::string getName();
    bool isClient();
private:
    bool isEditing;
    std::string name;
    bool client;
};


#endif //SEMESTRALNAPRACA_MENUSTART_H
