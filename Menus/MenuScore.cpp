//
// Created by jozef on 28. 12. 2020.
//

#include "MenuScore.h"
#include <fstream>
#include "MainMenu.h"
#include "../Game.h"

MenuScore::MenuScore() :
    Menu::Menu("SCORE", std::vector<std::string>()) {

    Menu::setMessage("Score 5 najlepsich hracov.");
    std::ofstream fl("score.dat", std::ofstream::out);
    fl<<"Jiyory 345\n";
    fl<<"Seagor 254\n";
    fl.close();

    std::ifstream file("score.dat");
    std::string name;
    int data;
    std::vector<std::string> players;
    while (file>>name) {
        file>>data;
        name.append(": ");
        name.append(std::to_string(data));
        players.push_back(name);
        if (players.size() >= 5) {
            break;
        }
    }
    for (int i = players.size(); i < 5; ++i) {
        players.push_back("Prazdna pozicia");
    }
    players.push_back("Predchadzajuce menu");
    Menu::replace(players);
    Menu::setActiveMap(std::vector<bool>({false, false, false, false, false, true}));
    Menu::setActive(5);
    Menu::setMessage("Pomocou ENTER sa vratite na predchadzajuce menu.");
}

MenuScore::~MenuScore() {

}

void MenuScore::onKeyPressCall(char key) {

}

void MenuScore::onMenuEnter(int choice) {
    Game::getGame()->setMenu(new MainMenu());
}
