//
// Created by jozef on 28. 12. 2020.
//

#ifndef SEMESTRALNAPRACA_MENUSCORE_H
#define SEMESTRALNAPRACA_MENUSCORE_H

#include <vector>
#include <string>

#include "Menu.h"

class MenuScore : public Menu {
public:
    MenuScore();
    ~MenuScore() override;
    void onKeyPressCall(char key) override;
    void onMenuEnter(int choice) override;
private:
};


#endif //SEMESTRALNAPRACA_MENUSCORE_H
