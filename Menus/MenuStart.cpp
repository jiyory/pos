//
// Created by jozef on 26. 12. 2020.
//

#include "MenuStart.h"
#include "MainMenu.h"
#include "../Game.h"
#include "MenuConnection.h"

MenuStart::MenuStart() :
    Menu::Menu("START", std::vector<std::string>({"Meno hraca:", "Typ pripojenia", "Predchadzajuce menu", "Zlate-Prasiatko", "Client", "Spustit"})) {

    this->setActiveMap(std::vector<bool>({true, true, true, false, false, true}));
    this->setMessage("Pomocou W,A,S,D sa pohybujete v menu. ENTER vyberie zvolenu moznost.");
    this->isEditing = false;
    this->client = true;
    this->name = "Zlate-Prasiatko";
}

MenuStart::~MenuStart() {
}

void MenuStart::onKeyPressCall(char key) {
    if (this->isEditing) {
        if (key != 10 && key != 32) { // space a enter
            if (key == 127) {
                this->name.erase(this->name.end() - 1);
            } else if ((key >= 'a' && key <= 'z') || (key >= 'A' && key <= 'Z') || (key >= '0' && key <= '9')) {
                if (this->name.length() < 30) {
                    this->name.push_back(key);
                }
            }
            Menu::change(3, this->name);
            Menu::show();
        }
    }
}

enum class CHOICES_MENUSTART {
    NAME = 0,
    CONN = 1,
    BACK = 2,
    NAMEINP = 3,
    CONNINP = 4,
    NEXT = 5
};

void MenuStart::onMenuEnter(int choice) {
    auto ch = (CHOICES_MENUSTART)(choice);
    if (ch == CHOICES_MENUSTART::NAME) {
        if (this->isEditing) {
            Menu::setActiveMap(std::vector<bool>({true, true, true, false, false, true}));
            if (this->name.length() < 1) {
                this->name = "Zlate-Prasiatko";
            }
            Menu::change(3, this->name);
            Menu::setMessage("Pomocou W,A,S,D sa pohybujete v menu. ENTER vyberie zvolenu moznost.");
        } else {
            Menu::setActiveMap(std::vector<bool>({true, false, false, false, false, false}));
            Menu::change(3, "");
            this->name = "";
            Menu::setMessage("Zadaj svoje herne meno. Povolene znaky su a-z, A-Z, 0-9.");
        }
        Menu::show();
        this->isEditing = !this->isEditing;
    } else if (ch == CHOICES_MENUSTART::CONN) {
        this->client = !this->client;
        if (this->client) {
            Menu::change(4, "Client");
        } else {
            Menu::change(4, "Host");
        }
        Menu::show();
    } else if (ch == CHOICES_MENUSTART::BACK) {
        Game::getGame()->setMenu(new MainMenu());
    } else if(ch == CHOICES_MENUSTART::NEXT) {
        Game::getGame()->setMenu(new MenuConnection(this->client, this->name));
    }
}

bool MenuStart::isClient() {
    return this->client;
}

std::string MenuStart::getName() {
    return this->name;
}
