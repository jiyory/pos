//
// Created by jozef on 6. 1. 2021.
//

#ifndef SEMESTRALNAPRACA_MENUFINAL_H
#define SEMESTRALNAPRACA_MENUFINAL_H

#include "Menu.h"

class MenuFinal : public Menu {
public:
    MenuFinal();
    virtual ~MenuFinal();
    void onKeyPressCall(char key) override;
    void onMenuEnter(int choice) override;
private:
};


#endif //SEMESTRALNAPRACA_MENUFINAL_H
