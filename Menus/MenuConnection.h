//
// Created by 958pe on 31. 12. 2020.
//

#ifndef SEMESTRALNAPRACA_MENUCONNECTION_H
#define SEMESTRALNAPRACA_MENUCONNECTION_H

#include "Menu.h"

class MenuConnection : public Menu {
public:
    MenuConnection(bool client, std::string &name);
    ~MenuConnection() override;
    void onKeyPressCall(char key) override;
    void onMenuEnter(int choice) override;
};


#endif //SEMESTRALNAPRACA_MENUCONNECTION_H
