//
// Created by jozef on 10. 11. 2020.
//

#ifndef SEMESTRALNAPRACA_MENU_H
#define SEMESTRALNAPRACA_MENU_H

#include <iostream>
#include <vector>
#include "letters.h"
#include "../Keys/keyboard.h"
#include "../Keys/OnKeyPress.h"

class Menu : public OnKeyPress {
public:
    Menu(const std::string& header, const std::vector<std::string>& items);
    virtual ~Menu();
    bool addNewItem(const std::string &item);
    bool addNewItems(const std::vector<std::string> &items);
    bool show() const;
    bool change(const unsigned short &position, const std::string &item);
    bool remove(const unsigned short &position);
    bool replace(const std::vector<std::string> items);
    bool header(const std::string &header);
    virtual void onKeyPressCall(char key) = 0;
    virtual void onMenuEnter(int choice) = 0;
    void setActiveMap(const std::vector<bool> &map);
    void setMessage(const std::string &message);
    void onKeyPress(char key) override;
    void setActive(int pos);
private:
    std::vector<std::string> items;
    std::vector<bool> map;
    std::string head;
    short active;
    std::string message;
    void printSpace(std::string &menu) const;
    void printHeader(std::string &menu) const;
    void printButtons(std::string &menu) const;
    void printMessage(std::string &menu) const;
};


#endif //SEMESTRALNAPRACA_MENU_H
