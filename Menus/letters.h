//
// Created by Peter on 10. 11. 2020.
//

#ifndef SEMESTRALNAPRACA_LETTERS_H
#define SEMESTRALNAPRACA_LETTERS_H

#include <string>

const std::string letters[][5] = {
    {
        "  X X  ",
        "X     X",
        "X     X",
        "X X X X",
        "X     X"
    },
    {
        "X X X  ",
        "X     X",
        "X X X  ",
        "X     X",
        "X X X  "
    },
    {
        "  X X  ",
        "X     X",
        "X      ",
        "X     X",
        "  X X  "
    },
    {
        "X X X  ",
        "X     X",
        "X     X",
        "X     X",
        "X X X  "
    },
    {
        "X X X X",
        "X      ",
        "X X X X",
        "X      ",
        "X X X X"
    },
    {
        "X X X X",
        "X      ",
        "X X X  ",
        "X      ",
        "X      "
    },
    {
        "  X X  ",
        "X     X",
        "X      ",
        "X   X X",
        "  X X  "
    },
    {
        "X     X",
        "X     X",
        "X X X X",
        "X     X",
        "X     X"
    },
    {
        " X X X ",
        "   X   ",
        "   X   ",
        "   X   ",
        " X X x "
    },
    {
        "      X",
        "      X",
        "      X",
        "X     X",
        "  X X  "
    },
    {
        "X     X",
        "X   X  ",
        "X X    ",
        "X   X  ",
        "X     X"
    },
    {
        "X      ",
        "X      ",
        "X      ",
        "X      ",
        "X X X X"
    },
    {
        "X     X",
        "X X X X",
        "X  X  X",
        "X     X",
        "X     X"
    },
    {
        "X     X",
        "X X   X",
        "X   X X",
        "X     X",
        "X     X"
    },
    {
        "  X X  ",
        "X     X",
        "X     X",
        "X     X",
        "  X X  "
    },
    {
        "X X X  ",
        "X     X",
        "X X X  ",
        "X      ",
        "X      "
    },
    {
        "  X X  ",
        "X     X",
        "X     X",
        "X   Q X",
        "  X X Q"
    },
    {
        "X X X  ",
        "X     X",
        "X X X  ",
        "X   X  ",
        "X     X"
    },
    {
        "X X X X",
        "X      ",
        "X X X X",
        "      X",
        "X X X X"
    },
    {
        "X X X X",
        "   X   ",
        "   X   ",
        "   X   ",
        "   X   "
    },
    {
        "X     X",
        "X     X",
        "X     X",
        "X     X",
        "X X X X"
    },
    {
        "X     X",
        "X     X",
        "X     X",
        "  X X  ",
        "   X   "
    },
    {
        "X  X  X",
        "X  X  X",
        "X  X  X",
        " X X X ",
        "  X X  "
    },
    {
        "X     X",
        "  X X  ",
        "   X   ",
        "  X X  ",
        "X     X"
    },
    {
        "X     X",
        "X     X",
        "  X X  ",
        "   X   ",
        "   X   "
    },
    {
        "X X X X",
        "      X",
        "  X X  ",
        "X      ",
        "X X X X"
    }
};

#endif //SEMESTRALNAPRACA_LETTERS_H
