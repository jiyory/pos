//
// Created by 958pe on 31. 12. 2020.
//

#include "MenuConnection.h"
#include "MenuStart.h"
#include "../Game.h"
#include <vector>
#include "../Conn/Client.h"
#include "../Conn/Server.h"

MenuConnection::MenuConnection(bool client, std::string &name) :
    Menu::Menu("Connect", std::vector<std::string>({"", "", "Predchadzajuce menu", "", "", "Spustit hru"})) {

    Game::getGame()->createPlayer(name);
    Menu::setActiveMap(std::vector<bool>({false, false, true, false, false, true}));
    Menu::setActive(2);
    if (!client) {
        Menu::setMessage("Spustam server...");
        Game::getGame()->runServer();
        Menu::setMessage("Cakam na hracov...");
        //if (!Game::getGame()->getServer()->isRunning()) {
        //Menu::setMessage("Nepodarilo sa spustit server. Skuste to neskor.");
        //}

    }
    Game::getGame()->connect();
    if (!Game::getGame()->getClient()->isRunningReading()) {
        //if (client || (Game::getGame()->getServer() != nullptr && Game::getGame()->getServer()->isRunning()))
        Menu::setMessage("Nepodarilo sa spustit klienta. Skuste to neskor.");
    }
}

MenuConnection::~MenuConnection() {
    //Game::getGame()->stopServer();
    //Game::getGame()->stopConnect();
}

void MenuConnection::onKeyPressCall(char key) {

}

enum class CHOICES_CONNECTION {
    PLAYER1 = 0,
    PLAYER2 = 1,
    BACK = 2,
    PLAYER3 = 3,
    PLAYER4 = 4,
    START = 5
};

void MenuConnection::onMenuEnter(int choice) {
    auto ch = (CHOICES_CONNECTION)(choice);
    switch (ch) {
        case CHOICES_CONNECTION::BACK: {
            Game::getGame()->stopConnect(true);
            Game::getGame()->stopServer();
            Game::getGame()->setMenu(new MenuStart());
            break;
        }
        case CHOICES_CONNECTION::START: {
            if (Game::getGame()->isServerOn()) {
                if (!Game::getGame()->runGame()) {
                    Menu::setMessage("Nedostatok hracov.");
                    Menu::show();
                }
            } else {
                Menu::setMessage("Nehostujes tuto hru! Cakaj na spustenie hry.");
                Menu::show();
            }
            break;
        }
    }
}
