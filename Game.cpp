//
// Created by jozef on 26. 12. 2020.
//

#include <unistd.h>
#include "Game.h"
#include "Conn/Server.h"
#include "Conn/Client.h"
#include "Menus/MenuConnection.h"
#include "Conn/CmdListCall.h"
#include <map>
#include "Map/GameManager.h"
#include "Map/Hrac.h"
#include "Menus/MenuFinal.h"

Game* Game::game = nullptr;

Game::Game() : menu(nullptr) {
    this->cmdCall = new CmdListCall();
    this->players = std::map<int, Hrac*>();
    this->server = nullptr;
    this->player = nullptr;
    this->client = nullptr;
}

/*
 * Odstrani menu ktore je aktualne vytvorene
 */
Game::~Game() {
    delete this->menu;
    delete this->server;
    delete this->client;
    delete this->player;
    for (std::pair<int, Hrac*> item : this->players) {
        if (item.second != player)
            delete item.second;
    }
    delete this->cmdCall;
}

/*
 * Odstrani povodne menu
 * Treba sem posielat new Menu...
 */
void Game::setMenu(Menu *nmenu) {
    if (this->menu != nullptr) {
        Keyboard::getKeyboard()->removeListener(this->menu);
    }
    this->menu = nmenu;
    if (this->menu != nullptr) {
        this->menu->show();
    }
}

Game *Game::getGame() {
    if (Game::game == nullptr) {
        Game::game = new Game();
    }
    return Game::game;
}

void Game::runServer() {
    try {
        this->server = new Server(7772);
    } catch (std::logic_error e) {
        std::cout<<"Nepodarilo sa spustit server.";
    }
}

Server* Game::getServer() {
    return this->server;
}

void Game::stopServer() {
    if (this->server != nullptr) {
        this->server->stop();
        delete this->server;
        this->server = nullptr;
    }
}

void Game::connect() {
    this->client = new Client("localhost", 7772);
}

Client* Game::getClient() {
    return this->client;
}

void Game::stopConnect(bool wait) {
    if (this->client != nullptr) {
        this->client->stopReading();
        if (wait) {
            delete this->client;
            this->client = nullptr;
        }
    }
}

void Game::addPlayer(int connection) {
    if (this->players.size() >= 4) {
        close(connection); // odpoji hraca
        return;
    }

    Hrac* hr = new Hrac(5,5, "...");
    this->players.insert({connection, hr});
    std::string temp = "getData ";
    std::vector<std::string> data = this->server->sendReturnMessage(connection, temp);
    this->players[connection]->setName(data[0]);
    this->players[connection]->setHP(std::stoi(data[1]));

    /* Aby neexistoval 2x */
    if (this->players.size() == 1) {
        delete this->player;
        this->player = this->players[connection];
        this->myConnection = connection;
    }
    // name, hp
    std::vector<std::string> play;
    for (std::pair<int, Hrac*> item : this->players) {
        play.emplace_back(item.second->getName());
        std::cout<<item.second->getName()<<"\n";
    }
    for (int i = play.size(); i < 5; ++i) {
        play.emplace_back("..."); // doplni prazdne miesta
    }
    this->cmdCall->sendInfoAboutPlayers(this->server, play);
    cmdCall->clientId(this->server, connection);
}

void Game::createPlayer(std::string &name) {
    delete this->player;
    this->player = new Hrac(1, 1, name);
}

Hrac *Game::getPlayer() {
    return this->player;
}

void Game::removePlayer(int connection) {
    auto item = this->players.find(connection);
    if (item != this->players.end()) {
        if (this->myConnection == connection) {
            this->player = nullptr;
        }
        delete item->second;
        this->players.erase(item);
    }
    std::vector<std::string> play;
    for (auto item : this->players) {
        play.emplace_back(item.second->getName());
    }
    for (int i = play.size(); i < 5; ++i) {
        play.emplace_back("..."); // doplni prazdne miesta
    }
    this->cmdCall->sendInfoAboutPlayers(this->server, play);
}

Menu *Game::getMenu() {
    return this->menu;
}

bool Game::isServerOn() {
    if (this->server) {
        return true;
    }
    return false;
}

bool Game::runGame() {
    if (this->players.size() < 2) {
        this->menu->setMessage("Nedostatok hracov pre spustenie hry!");
        return false;
    }
    this->cmdCall->runGame(this->server);
    return true;
}

void Game::createGame() {
    int i = 0;
    std::cout << "create game\n";
    this->setMenu(nullptr);

    GameManager* gm = GameManager::getManager();
    for (auto item : this->players) {
        if (i == 0) {
            item.second->setPos(1, 1);
        } else if (i == 1) {
            item.second->setPos(78, 1);
        } else if (i == 2) {
            item.second->setPos(1, 17);
        } else if (i == 3) {
            item.second->setPos(78, 17);
        }
        gm->addPlayer(item.second);
        ++i;
    }
    gm->start();
}

std::string Game::getPlayersInfo() {
    std::string pl;
    pl.append(std::to_string(this->players.size()));
    pl.append(" ");
    for (auto item : this->players) {
        Hrac* hrac = item.second;
        pl.append(std::to_string(item.first));
        pl.append(" ");
        pl.append(std::to_string(hrac->getPosX()));
        pl.append(" ");
        pl.append(std::to_string(hrac->getPosY()));
        pl.append(" ");
        pl.append(std::to_string(hrac->getHP()));
        pl.append(" ");
        pl.append(std::to_string(hrac->getCountKills()));
        pl.append(" ");
        pl.append(std::to_string(hrac->getCountDeaths()));
        pl.append(" ");
    }
    return pl;
}

void Game::setPlayerData(int i, int posx, int posy, int hp, int kills, int deaths) {
    this->lock();
    auto item = this->players.find(i);
    if (item != this->players.end()) {
        item->second->setPos(posx, posy);
        if (hp != INVALID_AMOUNT)
            item->second->setHP(hp);
        if (kills != INVALID_AMOUNT) // todo nefacha
            item->second->setCountKills(kills);
        if (deaths != INVALID_AMOUNT)
            item->second->setCountDeaths(deaths);

    } else {
        Hrac* hrac;
        if (i != myConnection) {
            hrac = new Hrac(posx, posy, "...");
        } else {
            hrac = player;
            hrac->setPos(posx, posy);
            if (hp != INVALID_AMOUNT)
                hrac->setHP(hp);
            if (kills != INVALID_AMOUNT)
                hrac->setCountKills(kills);
            if (deaths != INVALID_AMOUNT)
                hrac->setCountDeaths(deaths);
            GameManager::getManager()->changePlayer(player);
        }
        this->players.insert({i, hrac});
        GameManager::getManager()->addPlayer(hrac);
    }
    this->unlock();
}

void Game::setMyConnection(int id) {
    myConnection = id;
}

void Game::stopGame() {
    this->cmdCall->stopGame(this->client);
}

void Game::destroyGame() {
    GameManager* gm = GameManager::getManager();
    delete gm;
    this->setMenu(new MenuFinal());
}

std::string Game::getFinalStat() {
    std::string str;
    for (auto item : this->players) {
        str.append(" ");
        str.append(item.second->getName());
        str.append(":");
        str.append(std::to_string(item.second->getCountKills() * 20 - item.second->getCountDeaths() * 10));
    }
    return str;
}

void Game::addHp(int id, int value) {
    this->lock();
    auto hracIter = players.find(id);
    if (hracIter != players.end()) {
        hracIter->second->setHP(hracIter->second->getHP() + value);
    }
    this->unlock();
    checkEndGame();
}

void Game::checkEndGame() {
    for (auto item : this->players) {
        if (item.second->getHP() <= 0)
            stopGame();
    }
}

int Game::getMyConnection() {
    return myConnection;
}

void Game::incScore(int id) {
    this->lock();
    auto hracIter = players.find(id);
    if (hracIter != players.end()) {
        hracIter->second->increaseKills();
    }
    this->unlock();
}

void Game::lock() {
    pthread_mutex_lock(&this->mutex);
}

void Game::unlock() {
    pthread_mutex_unlock(&this->mutex);
}


