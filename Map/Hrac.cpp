//
// Created by 958pe on 28. 12. 2020.
//

#include "Hrac.h"

Hrac::Hrac(int x, int y, std::string name) {
    pos_x = x;
    pos_y = y;
    this->name = name;
    this->hp = 5;
}

Hrac::~Hrac() {

}

int Hrac::getPosX() const {
    return pos_x;
}

int Hrac::getPosY() const {
    return pos_y;
}

void Hrac::setPosX(int posX) {
    pos_x = posX;
}

void Hrac::setPosY(int posY) {
    pos_y = posY;
}

std::string &Hrac::getName() {
    return this->name;
}

int Hrac::getHP() {
    return this->hp;
}

void Hrac::setName(std::string name) {
    this->name = name;
}

void Hrac::setHP(int hp) {
    this->hp = hp;
}

int Hrac::getCountKills() const {
    return countKills;
}

int Hrac::getCountDeaths() const {
    return countDeaths;
}

void Hrac::increaseKills() {
    countKills++;
}

void Hrac::increaseDeaths() {
    countDeaths++;
}

void Hrac::setPos(int posX, int posY) {
    this->pos_x = posX;
    this->pos_y = posY;
}

void Hrac::setCountKills(int countKills) {
    this->countKills = countKills;
}

void Hrac::setCountDeaths(int countDeaths) {
    this->countDeaths = countDeaths;
}
