//
// Created by 958pe on 3. 1. 2021.
//

#ifndef SEMESTRALNAPRACA_GAMEMANAGER_H
#define SEMESTRALNAPRACA_GAMEMANAGER_H

#include <vector>
#include "Item.h"
#include "Movement.h"
#include "ItemShot.h"
#include "ItemFirstAid.h"
#include "Map.h"
#include "SelectMaps.h"
#include "../Conn/CmdListCall.h"

class GameManager {
public:
    static GameManager* getManager();
    virtual ~GameManager();
    void start();
    bool isRunning();
    void show();
    void makeTik();
    void vytvorStrelu(Direction smer);
    void vytvorStreluPos(int posx, int posy, int id, Direction smer = Direction::NONE);
    void vytvorFirstAid(int x = 0, int y = 0);
    void vytvorFirstAidClient(int x, int y);
    void zmazItem(Item* item);
    void zmazItemPos(int x, int y);
    void stop();
    void addPlayer(Hrac* hrac);
    std::string makeInfo();
    void infoFromServer(std::vector<std::string> params);
    void movePlayer(int x, int y);
    void changePlayer(Hrac* hrac);
private:
    std::vector<ItemShot*> zoznamStriel;
    std::vector<Item*> zoznamItemov;
    bool spustenaHral;
    Map* mapa;
    Movement* pohyb;
    SelectMaps* zoznamMap;
    pthread_t run;
    CmdListCall* cmdCall;

    static GameManager* manager;
    GameManager();
    //pthread_mutex_t mutex;
};


#endif //SEMESTRALNAPRACA_GAMEMANAGER_H
