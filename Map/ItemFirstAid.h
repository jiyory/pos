//
// Created by 958pe on 4. 1. 2021.
//

#ifndef SEMESTRALNAPRACA_ITEMFIRSTAID_H
#define SEMESTRALNAPRACA_ITEMFIRSTAID_H


#include "Item.h"

class ItemFirstAid : public Item {
public:
    ItemFirstAid(int x, int y);

    void use(Hrac *hrac) override;

};


#endif //SEMESTRALNAPRACA_ITEMFIRSTAID_H
