//
// Created by 958pe on 28. 12. 2020.
//

#include "Map.h"
#include "GameManager.h"

Map::Map(SelectMaps* select, Hrac* aktHrac) {
    this->select = select;
    auto walls = this->select->getRamdomMap();
    for(Item* temp : walls) {
        addItem(temp);
        //std::cout<<temp->getPosX() << " " << temp->getPosY() << "\n";
    }
    this->aktHrac = aktHrac;
    this->id = select->getNumberMap();
}

void Map::show() {
    for(int j = 0;j < 65; ++j)
        std::cout << "\n";

    for(int j = 0;j < 40 - (aktHrac->getName().size()/2); ++j)
        std::cout << " ";
    std::cout << aktHrac->getName() << "\n";
    std::cout << "Počet zabitých: " << aktHrac->getCountKills() << "               Počet smrtí: " << aktHrac->getCountDeaths()
    << "               Počet životov: " << aktHrac->getHP() << "\n\n";

    for(int j = 0;j < vyska; ++j) {
        for (int i = 0; i < sirka; ++i) {
            if (!checkRange(i, j)) {
                if((i == 0 || i == sirka - 1) && (j != 0 && j!= vyska - 1))
                    std::cout << "|";
                else {
                    std::cout << "=";
                }
                continue;
            }
            Item *item = getItem(i, j);
            if (item != nullptr) {
                std::cout << item->getZnak();
                continue;
            }
            Hrac *hrac = getHrac(i, j);
            if (hrac != nullptr) {
                std::cout << "H";
                continue;
            }
            std::cout << " ";
        }
        std::cout << "\n";
    }
    std::cout << "    WASD/šipky: pohyb  medzerník: výstrel  F: použitie lekárničky(max 6 hp)\n";
}

bool Map::checkKolision(int pos_x, int pos_y) {
    if(!checkRange(pos_x, pos_y))
        return true;
    if(getItem(pos_x, pos_y) != nullptr)
        return true;

    for(int i = 0;i < poleHracov.size(); ++i)
    {
        if(poleHracov[i]->getPosX() == pos_x && poleHracov[i]->getPosY() == pos_y)
            return true;
    }
    return false;
}

//false ak je v mimo rozsah
bool Map::checkRange(int pos_x, int pos_y) {
    if(pos_x > 78 || pos_x < 1 || pos_y > 17 || pos_y < 1)
        return false;
    return true;
}

void Map::addItem(Item *item) {
    poleItemov.push_back(item);
}

void Map::removeItem(Item* item) {
    for(int i = 0;i < poleItemov.size(); ++i)
    {
        if (poleItemov[i] == item)
        {
            poleItemov.erase(poleItemov.begin() + i);
        }
    }
}

Item *Map::getItem(int pos_x, int pos_y) {
    for(int i = 0;i < poleItemov.size(); ++i)
    {
        if (poleItemov[i]->getPosX() == pos_x && poleItemov[i]->getPosY() == pos_y)
            return poleItemov[i];
    }
    return nullptr;
}

void Map::addPlayer(Hrac *hrac) {
    poleHracov.push_back(hrac);
}

/*void Map::removePlayer() {
    for(int i = 0;i < poleHracov.size(); ++i)
    {
        if (poleHracov[i]->getHP() <= 0)
        {
            poleHracov.erase(poleHracov.begin() + i);
        }
    }
}*/

Hrac *Map::getHrac(int pos_x, int pos_y) {
    for(int i = 0;i < poleHracov.size(); ++i)
    {
        if (poleHracov[i]->getPosX() == pos_x && poleHracov[i]->getPosY() == pos_y)
            return poleHracov[i];
    }
    return nullptr;
}

int Map::getID() {
    return this->id;
}

void Map::changeMap(int id) {
    this->poleItemov.resize(0);
    for(Item* temp: this->select->getMapById(id))
        addItem(temp);
}

void Map::changePlayer(Hrac *hrac) {
    this->aktHrac = hrac;
}




