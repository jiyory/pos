//
// Created by 958pe on 3. 1. 2021.
//

#ifndef SEMESTRALNAPRACA_ITEMSHOT_H
#define SEMESTRALNAPRACA_ITEMSHOT_H

#include "Item.h"
#include "Movement.h"

class ItemShot : public Item {
public:
    ItemShot(Direction direct, int x, int y, Map* mapa, int id);
    void use(Hrac* hrac) override;
    bool move();
    virtual ~ItemShot();

private:
    Direction direct;
    Map* map;
    int id;
    bool checkKolision(int zvysX, int zvysY);
};


#endif //SEMESTRALNAPRACA_ITEMSHOT_H
