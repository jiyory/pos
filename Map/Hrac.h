//
// Created by 958pe on 28. 12. 2020.
//

#ifndef SEMESTRALNAPRACA_HRAC_H
#define SEMESTRALNAPRACA_HRAC_H

#include <string>

class Hrac {
public:
    Hrac(int x, int y, std::string name);
    ~Hrac();

    int getPosX() const;
    int getPosY() const;
    void setPosX(int posX);
    void setPosY(int posY);
    void setPos(int posX, int posY);
    std::string& getName();

    int getHP();

    void setName(std::string name);
    void setHP(int hp);
    int getCountKills() const;
    int getCountDeaths() const;
    void increaseKills();
    void increaseDeaths();

    void setCountKills(int countKills);
    void setCountDeaths(int countDeaths);

private:
    int pos_x;
    int pos_y;
    std::string name;
    int hp;
    int countKills = 0;
    int countDeaths = 0;

};


#endif //SEMESTRALNAPRACA_HRAC_H
