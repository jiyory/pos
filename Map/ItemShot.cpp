//
// Created by 958pe on 3. 1. 2021.
//

#include <unistd.h>
#include "ItemShot.h"
#include "../Game.h"

ItemShot::ItemShot(Direction direct, int x, int y, Map* mapa, int id): Item(x, y, '*')  {
    this->id = id;
    this->direct = direct;
    map = mapa;
}

void ItemShot::use(Hrac* hrac) {
    hrac->setHP(hrac->getHP() - 1);
    Game::getGame()->incScore(id);
    hrac->increaseDeaths();

    Game::getGame()->checkEndGame();

    //map->removePlayer();
}

ItemShot::~ItemShot() {

}

bool ItemShot::move() {
    if (direct == Direction::NONE) {
        return true;
    }
    if (direct == Direction::DOWN) {
        return !checkKolision(0, 1);
    } else if (direct == Direction::UP) {
        return !checkKolision(0, -1);
    } else if (direct == Direction::RIGHT) {
        return !checkKolision(1, 0);
    } else {
        return !checkKolision(-1, 0);
    }
}

bool ItemShot::checkKolision(int zvysX, int zvysY) {
    if (!map->checkKolision(getPosX() + zvysX, getPosY() + zvysY)) {
        setPos(getPosX() + zvysX, getPosY() + zvysY);
        return false;
    } else {
        Hrac *tempHrac = map->getHrac(getPosX() + zvysX, getPosY() + zvysY);
        if (tempHrac != nullptr) {
            use(tempHrac);
        }
    }
    return true;
}




