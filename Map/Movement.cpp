//
// Created by 958pe on 30. 12. 2020.
//

#include "Movement.h"
#include "GameManager.h"
#include "ItemWall.h"
#include "../Game.h"

Movement::Movement(Map* mapa, Hrac* hrac) {
    this->mapa = mapa;
    this->hrac = hrac;
    start = std::chrono::system_clock::now();
    smer = Direction::DOWN;
}

bool Movement::move(Direction direct, Hrac* hrac) {
    if(direct == Direction::DOWN)
    {
        if(!mapa->checkKolision(hrac->getPosX(), hrac->getPosY() + 1))
        {
            hrac->setPosY(hrac->getPosY() + 1);
            return true;
        }
    } else if(direct == Direction::UP)
        {
            if(!mapa->checkKolision(hrac->getPosX(), hrac->getPosY() - 1))
            {
                hrac->setPosY(hrac->getPosY() - 1);
                return true;
            }

        } else if(direct == Direction::RIGHT)
            {
                if(!mapa->checkKolision(hrac->getPosX() + 1, hrac->getPosY()))
                {
                    hrac->setPosX(hrac->getPosX() + 1);
                    return true;
                }

            } else
            {
                if(!mapa->checkKolision(hrac->getPosX() - 1, hrac->getPosY()))
                {
                    hrac->setPosX(hrac->getPosX() - 1);
                    return true;
                }
            }
    return false;
}

Item *Movement::takeItem() {
    Item* temp;
    ItemWall* wall;

    temp = mapa->getItem(hrac->getPosX(), hrac->getPosY() + 1);
    wall = dynamic_cast<ItemWall*>(temp);
    if (wall == nullptr)
         if(temp != nullptr)
            return temp;

    temp = mapa->getItem(hrac->getPosX(), hrac->getPosY() - 1);
    wall = dynamic_cast<ItemWall*>(temp);
    if (wall == nullptr)
        if(temp != nullptr)
            return temp;

    temp = mapa->getItem(hrac->getPosX() + 1, hrac->getPosY());
    wall = dynamic_cast<ItemWall*>(temp);
    if (wall == nullptr)
        if(temp != nullptr)
            return temp;

    temp = mapa->getItem(hrac->getPosX() - 1, hrac->getPosY());
    wall = dynamic_cast<ItemWall*>(temp);
    if (wall == nullptr)
        if(temp != nullptr)
            return temp;
    return nullptr;
}

void Movement::onKeyPress(char key) {
    GameManager* manager = GameManager::getManager();
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> rozdielCas = end - start;
    if(rozdielCas.count() > 0.2) {
        Game::getGame()->lock();
        if (key == 119 || key == 65) {
            if (move(Direction::UP, hrac))
            {
                manager->movePlayer(hrac->getPosX(), hrac->getPosY());
                start = std::chrono::system_clock::now();
                smer = Direction::UP;
            }

        } else if (key == 115 || key == 66) {
            if (move(Direction::DOWN, hrac))
            {
                manager->movePlayer(hrac->getPosX(), hrac->getPosY());
                start = std::chrono::system_clock::now();
                smer = Direction::DOWN;
            }
        } else if (key == 97 || key == 68) {
            if(move(Direction::LEFT, hrac))
            {
                manager->movePlayer(hrac->getPosX(), hrac->getPosY());
                start = std::chrono::system_clock::now();
                smer = Direction::LEFT;
            }
        } else if (key == 100 || key == 67) {
            if(move(Direction::RIGHT, hrac))
            {
                manager->movePlayer(hrac->getPosX(), hrac->getPosY());
                start = std::chrono::system_clock::now();
                smer = Direction::RIGHT;
            }
        } else if (key == 32) {
            manager->vytvorStrelu(smer);
            start = std::chrono::system_clock::now();
        } else if (key == 102) {
            ItemFirstAid *item = dynamic_cast<ItemFirstAid *>(takeItem());
            if (item != nullptr && hrac->getHP() < 6) {
                manager->vytvorFirstAid(item->getPosX(), item->getPosY());
                item->use(hrac);
                manager->zmazItem(item);
            }
        } else if (key == 27) {
            start = std::chrono::system_clock::now();
            Game::getGame()->stopGame();
        }
        Game::getGame()->unlock();
    }
}

Movement::~Movement() {
}

void Movement::changePlayer(Hrac *hrac) {
    this->hrac = hrac;
}

