//
// Created by 958pe on 2. 1. 2021.
//

#include "SelectMaps.h"
#include "ItemWall.h"

SelectMaps::SelectMaps() {
    srand(time(NULL));
    std::string mapa = "                |                                                 |           "
                       "     |          |                        |                        |           "
                       "     |          |=============    =======|           |            |           "
                       "     |          |                        |           |            |           "
                       "     |          |==  ============================  ==|            |           "
                       "     |          |          |              |          |            |           "
                       "     |==  ==  ==|          |              |          |=======  ===========  =="
                       "                |          |      |       |          |                        "
                       "                |                 |                  |                        "
                       "                                  |                  |                        "
                       "  ==================  ==========================  =======  ==================="
                       "                                                                              "
                       "                                                                              "
                       "===========  ===========================  ========  =================  ======="
                       "                                               |                    |         "
                       "                        |                      |                    |         "
                       "                        |                      |                              ";
    addMap(translateMap(mapa));

    mapa = "  |                                                                           "
           "  |     |=========================          =========================|      =="
           "        |                                                            |        "
           "     ====                                                            ====     "
           "                                                                              "
           "                        ===============================                       "
           "                        |                             |                       "
           "                        |      |      ====      |     |                       "
           "=============                  |                |                ============="
           "                        |      |      ====      |     |                       "
           "                        |                             |                       "
           "                        ===============================                       "
           "                                                                              "
           "     ====                                                           ====      "
           "        |                                                           |         "
           "==      |=========================         =========================|      |  "
           "                                                                           |  ";
    addMap(translateMap(mapa));

    mapa = "           |                    |==                       |==     |           "
           "                                                                              "
           "           |    |======                                           |           "
           "                                      |==                                     "
           "= = = = = =|                                                      |= = = = = ="
           "               |==                                                            "
           "           |                                         |==        ==|           "
           "                                                                              "
           "     =     |                             |======           |==    |     =     "
           "                           |==                                                "
           "           |==                                                    |           "
           "                                                                              "
           "= = = = = =|                                      |==             |= = = = = ="
           "               |==                                                            "
           "           |             |==         |==                          |           "
           "                                                                              "
           "           |                                      |==             |           ";
    addMap(translateMap(mapa));
}

SelectMaps::~SelectMaps() {
    for(int i = 0; i < poleMap.size(); ++i)
    {
        for(Item* temp : poleMap[i])
        {
            delete temp;
        }
    }
}


std::vector<Item *> SelectMaps::getRamdomMap() {
    aktMapa = rand() % poleMap.size();
    return poleMap[aktMapa];
}

std::vector<Item *> SelectMaps::translateMap(std::string mapa) {
    //80 19 pre mna 78 17
    std::vector<Item *> tempPole;

    for(int i = 0; i < mapa.size(); ++i)
    {
        if (mapa[i] != ' ')
        {
            Item* item = new ItemWall((i%78) + 1, (i/78) + 1, mapa[i]);
            tempPole.push_back(item);
        }
    }
    return tempPole;
}

void SelectMaps::addMap(std::vector<Item *> mapa) {
    poleMap.push_back(mapa);
}

int SelectMaps::getNumberMap() {
    return aktMapa;
}

std::vector<Item *> SelectMaps::getMapById(int id) {
    return this->poleMap[id];
}

