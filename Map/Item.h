//
// Created by 958pe on 28. 12. 2020.
//

#ifndef SEMESTRALNAPRACA_ITEM_H
#define SEMESTRALNAPRACA_ITEM_H


#include "Hrac.h"

class Item {
public:
    Item(int x,int y,char znak);
    virtual void use(Hrac* hrac) = 0;

private:
    int pos_x;
    int pos_y;
    char znak;
public:
    int getPosX() const;

    int getPosY() const;

    void setPos(int posX, int  posY);

    char getZnak() const;


    void setZnak(char znak);

};


#endif //SEMESTRALNAPRACA_ITEM_H
