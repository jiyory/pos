//
// Created by 958pe on 4. 1. 2021.
//

#include "ItemFirstAid.h"

ItemFirstAid::ItemFirstAid(int x, int y) : Item(x, y, 'F') {}

void ItemFirstAid::use(Hrac *hrac) {
    hrac->setHP(hrac->getHP() + 1);
}
