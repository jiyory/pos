//
// Created by 958pe on 3. 1. 2021.
//

#ifndef SEMESTRALNAPRACA_ITEMWALL_H
#define SEMESTRALNAPRACA_ITEMWALL_H


#include "Item.h"

class ItemWall : public Item {
public:
    ItemWall(int x, int y, char znak);

    void use(Hrac* hrac) override;

};


#endif //SEMESTRALNAPRACA_ITEMWALL_H
