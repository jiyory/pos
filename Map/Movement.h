//
// Created by 958pe on 30. 12. 2020.
//

#ifndef SEMESTRALNAPRACA_MOVEMENT_H
#define SEMESTRALNAPRACA_MOVEMENT_H

#include <vector>
#include <chrono>
#include "Hrac.h"
#include "Map.h"
#include "../Keys/OnKeyPress.h"

enum class Direction { LEFT = 0,RIGHT = 1, UP = 2, DOWN = 3, NONE = 4};

class Movement : public OnKeyPress {
public:
    Movement(Map* mapa, Hrac* hrac);
    ~Movement() override;
    bool move(Direction direct, Hrac* hrac);
    Item* takeItem();
    void onKeyPress(char key) override;
    void changePlayer(Hrac* hrac);

private:
    Hrac* hrac;// mozno toto tam nebude
    Map* mapa;
    std::chrono::time_point<std::chrono::system_clock> start;
    Direction smer;
};


#endif //SEMESTRALNAPRACA_MOVEMENT_H
