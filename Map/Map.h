//
// Created by 958pe on 28. 12. 2020.
//

#ifndef SEMESTRALNAPRACA_MAP_H
#define SEMESTRALNAPRACA_MAP_H


#include "Item.h"
#include "Hrac.h"
#include <vector>
#include <iostream>
#include "SelectMaps.h"

class Map {
public:
    Map(SelectMaps* select, Hrac* hrac);
    void show();
    bool checkKolision(int pos_x, int pos_y);
    void addItem(Item *item);
    void removeItem(Item* item);
    void addPlayer(Hrac* hrac);
    //void removePlayer();
    Item* getItem(int pos_x, int pos_y);
    Hrac* getHrac(int pos_x, int pos_y);
    void changeMap(int id);
    int getID();
    void changePlayer(Hrac* hrac);
private://80 19
    int vyska = 19;
    int sirka = 80;
    std::vector<Item*> poleItemov;
    std::vector<Hrac*> poleHracov;
    Hrac* aktHrac;
    int id;
    SelectMaps* select;

    bool checkRange(int pos_x, int pos_y);

};


#endif //SEMESTRALNAPRACA_MAP_H
