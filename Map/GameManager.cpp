//
// Created by 958pe on 3. 1. 2021.
//

#include <unistd.h>
#include <iostream>
#include "GameManager.h"
#include "ItemShot.h"
#include "ItemFirstAid.h"
#include "SelectMaps.h"
#include "../Game.h"

GameManager* GameManager::manager = nullptr;

GameManager::GameManager() {
    //this->mutex = pthread_mutex_t();
    this->cmdCall = new CmdListCall();
    this->run = pthread_t();
    srand (time(NULL));
    spustenaHral = true;

    this->zoznamMap = new SelectMaps();
    this->mapa = new Map(this->zoznamMap, Game::getGame()->getPlayer());
    this->mapa->addPlayer(Game::getGame()->getPlayer());
    vytvorFirstAid();
    vytvorFirstAid();

    this->pohyb = new Movement(this->mapa, Game::getGame()->getPlayer());
    Keyboard *keys = Keyboard::getKeyboard();
    keys->addListener(this->pohyb);
}

GameManager::~GameManager() {
    this->stop();
    delete cmdCall;
    for(ItemShot* shot : zoznamStriel)
    {
        delete shot;
    }
    for(Item* firstAid : zoznamItemov)
    {
        delete firstAid;
    }
    delete this->mapa;
    delete this->zoznamMap;
    Keyboard::getKeyboard()->removeListener(this->pohyb); // vykona aj delete
}

GameManager *GameManager::getManager() {
    if(manager == nullptr)
        manager = new GameManager();
    return manager;
}

void * tik(void * param) {
    GameManager* gm = GameManager::getManager();
    while(gm->isRunning())
    {
        gm->makeTik();
        usleep(1000000/15);
    }
}

void GameManager::start() {
    pthread_create(&this->run, NULL, &tik, NULL);
}

void GameManager::vytvorStrelu(Direction smer) {
    Hrac *hrac = Game::getGame()->getPlayer();
    if (Game::getGame()->isServerOn()) {
        ItemShot *strela = new ItemShot(smer, hrac->getPosX(), hrac->getPosY(), this->mapa, Game::getGame()->getMyConnection());
        zoznamStriel.push_back(strela);
        mapa->addItem(strela);
    } else
    {
        std::string info;
        info.append(std::to_string(hrac->getPosX()));
        info.append(" ");
        info.append(std::to_string(hrac->getPosY()));
        info.append(" ");
        info.append(std::to_string((int)smer));
        cmdCall->sendInfoShot(Game::getGame()->getClient(), info);
    }

}

void GameManager::zmazItem(Item* item) {
    if (item == nullptr)
        return;
    for(int i = 0; i < zoznamStriel.size(); ++i)
    {
        if (zoznamStriel[i] == item)
        {
            mapa->removeItem(item);
            delete zoznamStriel[i];
            zoznamStriel.erase(zoznamStriel.begin() + i);
        }
    }
    for(int i = 0; i < zoznamItemov.size(); ++i)
    {
        if (zoznamItemov[i] == item)
        {
            mapa->removeItem(item);
            delete zoznamItemov[i];
            zoznamItemov.erase(zoznamItemov.begin() + i);
        }
    }
}

void GameManager::vytvorFirstAid(int x, int y) {
    if (Game::getGame()->isServerOn()) {
        int x = rand() % 78 + 1;
        int y = rand() % 17 + 1;
        if (!mapa->checkKolision(x, y)) {
            Item *firstAid = new ItemFirstAid(x, y);
            zoznamItemov.push_back(firstAid);
            this->mapa->addItem(firstAid);
            return;
        }
        vytvorFirstAid();
    } else if (x + y != 0) {
        std::string info;
        info.append(std::to_string(x));
        info.append(" ");
        info.append(std::to_string(y));
        cmdCall->sendInforUseFirstAid(Game::getGame()->getClient(), info);
    }
}

void GameManager::stop() {
    spustenaHral = false;
    pthread_join(this->run, NULL);
}

void GameManager::addPlayer(Hrac *hrac) {
    this->mapa->addPlayer(hrac);
}

bool GameManager::isRunning() {
    return this->spustenaHral;
}

void GameManager::show() {
    this->mapa->show();
}

void GameManager::makeTik() {
    if (Game::getGame()->isServerOn()) {
        for(ItemShot* shot : zoznamStriel)
        {
            if(!shot->move())
            {
                zmazItem(shot);
            }
        }
         this->cmdCall->sendInfoAboutGame(Game::getGame()->getServer(), Game::getGame()->getPlayersInfo(), this->makeInfo());
    }
    mapa->show();
}

std::string GameManager::makeInfo() {
    std::string info = std::to_string(zoznamMap->getNumberMap());
    info.append(" ");

    info.append(std::to_string(zoznamItemov.size()));

    for(int i = 0; i < zoznamItemov.size(); ++i)
    {
        info.append(" ");
        info.append(std::to_string(zoznamItemov[i]->getPosX()));
        info.append(" ");
        info.append(std::to_string(zoznamItemov[i]->getPosY()));
    }

    info.append(" ");
    info.append(std::to_string(zoznamStriel.size()));

    for(int i = 0; i < zoznamStriel.size(); ++i)
    {
        info.append(" ");
        info.append(std::to_string(zoznamStriel[i]->getPosX()));
        info.append(" ");
        info.append(std::to_string(zoznamStriel[i]->getPosY()));
        //info.append(" ");
        //smer strely 1 hore 2 dole 3 vpravo 4 vlavo
        //info.append(std::to_string(zoznamStriel[i]->getDirect()));
    }
    //std::cout<<"makeInfoEnd\n";
    return info;
}

void GameManager::infoFromServer(std::vector<std::string> params) {
    //std::cout<<"InfoFromServer\n";
    //for(auto item : params)
    //    std::cout << item << "\n";
    //std::cout<<"InfoFromServer\n";

    int pocetHracov = stoi(params[0]);
    for (int i = 0; i < pocetHracov; ++i) {
        int con = stoi(params[i*6 + 1]);
        int posx = stoi(params[i*6 + 2]);
        int posy = stoi(params[i*6 + 3]);
        int hp = stoi(params[i*6 + 4]);
        int kills = stoi(params[i*6 + 5]);
        int deaths = stoi(params[i*6 + 6]);
        Game::getGame()->setPlayerData(con, posx, posy, hp, kills, deaths);
    }
    int ns = pocetHracov * 6 + 1; // newStart
    int mapa = stoi(params[ns]);

    if (this->mapa->getID() != mapa) {
        this->mapa->changeMap(mapa);
    }


    for (Item* item : this->zoznamItemov) {
        this->mapa->removeItem(item);
        delete item;
    }
    this->zoznamItemov.resize(0);

    int pocetLek = stoi(params[ns + 1]);
    for (int i = 0; i < pocetLek; ++i) {
        int posx = stoi(params[ns + i*2 + 2]);
        int posy = stoi(params[ns + i*2 + 3]);
        this->vytvorFirstAidClient(posx, posy);
    }
    ns += pocetLek * 2 + 2;
    for (ItemShot* strela : this->zoznamStriel) {
        this->mapa->removeItem(strela);
        delete strela;
    }
    this->zoznamStriel.resize(0);
    int ps = stoi(params[ns]);
    for (int i = 0; i < ps; ++i) {
        int posx = stoi(params[ns + i*2 + 1]);
        int posy = stoi(params[ns + i*2 + 2]);
        this->vytvorStreluPos(posx, posy, Game::getGame()->getMyConnection());
    }
    //std::cout<<"InfoFromServerEnd\n";
}

void GameManager::vytvorFirstAidClient(int x, int y) {
    Item* firstAid = new ItemFirstAid(x, y);
    zoznamItemov.push_back(firstAid);
    this->mapa->addItem(firstAid);
}

void GameManager::vytvorStreluPos(int posx, int posy, int id, Direction smer) {
    ItemShot* strela = new ItemShot(smer, posx, posy, this->mapa, id);
    zoznamStriel.push_back(strela);
    mapa->addItem(strela);
}

void GameManager::zmazItemPos(int x, int y) {
    zmazItem(mapa->getItem(x,y));
}

void GameManager::movePlayer(int x, int y) {

    if(Game::getGame()->isServerOn())
        return;
    std::string info;
    info.append(std::to_string(x));
    info.append(" ");
    info.append(std::to_string(y));
    cmdCall->sendInfoPlayer(Game::getGame()->getClient(), info);
}

void GameManager::changePlayer(Hrac *hrac) {
    pohyb->changePlayer(hrac);
    mapa->changePlayer(hrac);
}



