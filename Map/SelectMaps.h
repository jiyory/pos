//
// Created by 958pe on 2. 1. 2021.
//

#ifndef SEMESTRALNAPRACA_SELECTMAPS_H
#define SEMESTRALNAPRACA_SELECTMAPS_H

#include <vector>
#include "Item.h"
#include <string>
#include <iostream>

class SelectMaps {
public:
    SelectMaps();
    virtual ~SelectMaps();
    std::vector<Item*> getRamdomMap();
    int getNumberMap();
    std::vector<Item*> getMapById(int id);
private:
    std::vector<std::vector<Item*>> poleMap;
    int aktMapa;

    std::vector<Item*> translateMap(std::string mapa);
    void addMap(std::vector<Item*> mapa);
};


#endif //SEMESTRALNAPRACA_SELECTMAPS_H
