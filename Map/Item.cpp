//
// Created by 958pe on 28. 12. 2020.
//

#include "Item.h"

Item::Item(int x, int y, char znak) {
    pos_x = x;
    pos_y = y;
    this->znak = znak;
}

int Item::getPosX() const {
    return pos_x;
}

int Item::getPosY() const {
    return pos_y;
}

char Item::getZnak() const {
    return znak;
}

void Item::setPos(int posX, int posY) {
    pos_x = posX;
    pos_y = posY;
}


void Item::setZnak(char znak) {
    Item::znak = znak;
}
