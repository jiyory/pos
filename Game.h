//
// Created by jozef on 26. 12. 2020.
//

#ifndef SEMESTRALNAPRACA_GAME_H
#define SEMESTRALNAPRACA_GAME_H

#include "Menus/Menu.h"
#include "Map/Hrac.h"
#include <map>
#include "Conn/CmdListCall.h"
class Server;
class Client;

#define INVALID_AMOUNT -5
class Game {
public:
    static Game* getGame();
    ~Game();
    void setMenu(Menu *nmenu);
    Menu* getMenu();
    void connect();
    void stopConnect(bool wait);
    Client* getClient();
    void runServer();
    void stopServer();
    Server* getServer();
    void addPlayer(int connection);
    void removePlayer(int connection);
    void createPlayer(std::string &name);
    Hrac* getPlayer();
    bool isServerOn();
    bool runGame();
    void setMyConnection(int id);

    /* After start */
    void createGame();
    std::string getPlayersInfo();
    void setPlayerData(int i, int posx, int posy, int hp = INVALID_AMOUNT, int kills = INVALID_AMOUNT, int deaths = INVALID_AMOUNT);
    void stopGame();
    void destroyGame();
    std::string getFinalStat();
    void addHp(int id, int value);
    void checkEndGame();
    int getMyConnection();
    void incScore(int id);

    void lock();
    void unlock();
private:
    Menu* menu;
    static Game * game;
    Game();
    Server* server;
    Client* client;
    std::map<int, Hrac*> players;
    Hrac* player;
    CmdListCall* cmdCall;
    int myConnection;
    pthread_mutex_t mutex;
};


#endif //SEMESTRALNAPRACA_GAME_H
