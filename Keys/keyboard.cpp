//
// Created by jozef on 10. 11. 2020.
//

#include "keyboard.h"
#include <iostream>
#include <termios.h>
#include <unistd.h>

Keyboard* Keyboard::kb = nullptr;

/*
 * Funkcia prevzata z webu:
 * https://stackoverflow.com/questions/421860/capture-characters-from-standard-input-without-waiting-for-enter-to-be-pressed
 */
char getch() {
    char buf = 0;
    struct termios old = {0};
    if (tcgetattr(0, &old) < 0)
        perror("tcsetattr()");
    old.c_lflag &= ~ICANON;
    old.c_lflag &= ~ECHO;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0)
        perror("tcsetattr ICANON");
    if (read(0, &buf, 1) < 0)
        perror ("read()");
    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old) < 0)
        perror ("tcsetattr ~ICANON");
    return (buf);
}

extern "C" void* run(void * obj) {
    Keyboard* kb = reinterpret_cast<Keyboard*>(obj);
    char c;
    char last;
    while (kb->isRunning()) {
        c = getch();
        kb->onKeyPress(c);
    }
}

void Keyboard::start() {
    this->_detect = true;
    pthread_create(&this->running, NULL, &run, this);
}

void Keyboard::stop() {
    this->_detect = false;
}

bool Keyboard::isRunning() {
    return this->_detect;
}

void Keyboard::onKeyPress(int key) {
    if (!this->listenersDelete.empty()) {
        for (OnKeyPress* item : this->listenersDelete) {
            int i = 0;
            for(auto find : this->listeners) {
                if (find == item) {
                    break;
                }
                ++i;
            }
            this->listeners.erase(listeners.begin() + i);
            delete item;
        }
        this->listenersDelete.resize(0);
    }
    if (!this->listenersAdd.empty()) {
        this->listeners.insert(this->listeners.end(), this->listenersAdd.begin(), this->listenersAdd.end());
        this->listenersAdd.resize(0);
    }
    for (OnKeyPress* item : this->listeners) {
        item->onKeyPress((char)key);
    }
}

void Keyboard::removeListener(OnKeyPress *listen) {
    int i = 0;
    for (OnKeyPress* item : this->listenersAdd) {
        if (item == listen) {
            break;
        }
        ++i;
    }
    if (i < listenersAdd.size()) {
        this->listenersAdd.erase(listenersAdd.begin() + i);
        delete listen;
    } else {
        listenersDelete.push_back(listen);
    }
}

void Keyboard::addListener(OnKeyPress *listen) {
    this->listenersAdd.push_back(listen);
}

Keyboard* Keyboard::getKeyboard() {
    if (Keyboard::kb == nullptr) {
        Keyboard::kb = new Keyboard();
    }
    return Keyboard::kb;
}

void Keyboard::waitForStop() {
    pthread_join(this->running, NULL);
}

Keyboard::Keyboard() {
    this->_detect = false;
    this->start();
}

Keyboard::~Keyboard() {
    this->_detect = false;
}
