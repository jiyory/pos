//
// Created by jozef on 25. 12. 2020.
//

#ifndef SEMESTRALNAPRACA_ONKEYPRESS_H
#define SEMESTRALNAPRACA_ONKEYPRESS_H


class OnKeyPress {
public:
    virtual ~OnKeyPress() {};
    virtual void onKeyPress(char key) = 0;
};


#endif //SEMESTRALNAPRACA_ONKEYPRESS_H
