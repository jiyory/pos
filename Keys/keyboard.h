//
// Created by jozef on 10. 11. 2020.
//

#ifndef SEMESTRALNAPRACA_KEYBOARD_H
#define SEMESTRALNAPRACA_KEYBOARD_H

#include <pthread.h>
#include "OnKeyPress.h"
#include <vector>

class Keyboard {
public:
    static Keyboard* getKeyboard();
    ~Keyboard();
    void onKeyPress(int key);
    bool isRunning();
    void addListener(OnKeyPress *listen);
    void removeListener(OnKeyPress *listen);
    void stop();
    void waitForStop();
private:
    Keyboard();
    void start();
    bool _detect;
    pthread_t running{};
    std::vector<OnKeyPress*> listeners;
    static Keyboard* kb;
    std::vector<OnKeyPress*> listenersAdd;
    //std::vector<int> listenersRemove;
    std::vector<OnKeyPress*> listenersDelete;
};


#endif //SEMESTRALNAPRACA_KEYBOARD_H
