//
// Created by jozef on 2. 1. 2021.
//

#include "ReturnMessage.h"
#include <iostream>

ReturnMessage::ReturnMessage() {
    pthread_mutex_init(&this->mutex, NULL);
    this->retValues = std::map<std::string, std::string>();
}

ReturnMessage::~ReturnMessage() {
    pthread_mutex_destroy(&this->mutex);
}

bool ReturnMessage::addMessage(std::string prefix, std::string param) {
    if (prefix == "exit") {
        return true;
    }
    pthread_mutex_lock(&this->mutex);
    this->retValues.insert(std::pair<std::string, std::string>(prefix, param));
    pthread_mutex_unlock(&this->mutex);
    return false;
}

std::string ReturnMessage::getMessage(std::string &prefix) {
    pthread_mutex_lock(&this->mutex);
    auto msg = this->retValues.find(prefix);
    std::string ret = msg->second;
    if (msg != this->retValues.end()) {
        //this->retValues.erase(prefix); // TODO memory?
        pthread_mutex_unlock(&this->mutex);
        return ret;
    } else {
        pthread_mutex_unlock(&this->mutex);
        return std::string();
    }
}
