//
// Created by jozef on 3. 1. 2021.
//

#include "CmdListCall.h"
#include "Server.h"
#include "Client.h"

void CmdListCall::sendInfoAboutPlayers(Server *server, std::vector <std::string> players) {
    std::string str = "menuConPlayers ";
    for (std::string name : players) {
        str.append(name);
        str.append(" ");
    }
    server->sendMessageToAll(str);
}

void CmdListCall::exitClient(Client *client) {
    std::string str = "exit ";
    client->sendMessage(str);
}

void CmdListCall::exitServer(Server *server) {
    std::string str = "exit ";
    server->sendMessageToAll(str);
}

void CmdListCall::runGame(Server *server) {
    std::string str = "runGame ";
    server->sendMessageToAll(str);
}

void CmdListCall::sendInfoAboutGame(Server *server, std::string players, std::string map) {
    std::string str = "sendInfoAboutGame ";
    str.append(players);
    str.append(map);
    //std::cout<<str<<"\n";
    server->sendMessageToAll(str);
}

void CmdListCall::sendInforUseFirstAid(Client *client, std::string position) {
    std::string str = "sendInforUseFirstAid ";
    str.append(position);
    client->sendMessage(str);
}

void CmdListCall::sendInfoShot(Client *client, std::string info) {
    std::string str = "sendInfoShot ";
    str.append(info);
    client->sendMessage(str);
}

void CmdListCall::sendInfoPlayer(Client *client, std::string position) {
    std::string str = "sendInfoPlayer ";
    str.append(position);
    client->sendMessage(str);
}

void CmdListCall::clientId(Server *server, int id) {
    std::string str = "clientId ";
    str.append(std::to_string(id));
    server->sendMessage(id, str);
}

void CmdListCall::stopGame(Client* client) {
    std::string str = "stopGame ";
    client->sendMessage(str);
}
