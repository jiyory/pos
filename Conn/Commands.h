//
// Created by jozef on 3. 1. 2021.
//

#ifndef SEMESTRALNAPRACA_COMMANDS_H
#define SEMESTRALNAPRACA_COMMANDS_H

#include <string>
#include <map>
#include <vector>
#include "CmdList.h"

class Server;
class Client;

class Commands {
public:
    Commands(bool client);
    ~Commands();
    void executeServer(std::string &cmd, Server* server, int client, std::string param);
    void executeClient(std::string &cmd, Client* client, std::string param);
private:
    std::map<std::string, void (CmdList::*)(Server*, int, std::vector<std::string>)> functionS;
    std::map<std::string, void (CmdList::*)(Client*, std::vector<std::string>)> functionC;
    CmdList* cmdList;
};


#endif //SEMESTRALNAPRACA_COMMANDS_H
