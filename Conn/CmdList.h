//
// Created by jozef on 3. 1. 2021.
//

#ifndef SEMESTRALNAPRACA_CMDLIST_H
#define SEMESTRALNAPRACA_CMDLIST_H

#include <vector>
#include <string>
class Client;
class Server;

class CmdList {
public:
    /* SERVER */
    void sendInfoUseFirstAid(Server* server, int client, std::vector<std::string> info);
    void sendInfoShot(Server* server, int client, std::vector<std::string> info);
    void sendInfoPlayer(Server* server, int client, std::vector<std::string> info);
    void stopGame(Server* server, int client, std::vector<std::string> info);

    /* CLIENT */
    void menuConPlayers(Client* client, std::vector<std::string> params);
    void getData(Client* client, std::vector<std::string> params);
    void runGame(Client* client, std::vector<std::string> params);
    void sendInfoAboutGame(Client* client, std::vector<std::string> params);
    void clientId(Client* client, std::vector<std::string> params);
    void endGame(Client* client, std::vector<std::string> params);
};


#endif //SEMESTRALNAPRACA_CMDLIST_H
