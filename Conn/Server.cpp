//
// Created by jozef on 25. 12. 2020.
//

#include "Server.h"
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include "../Game.h"
#include <bits/stdc++.h>
#include "Commands.h"

// zdroj: https://stackoverflow.com/questions/36153781/c-tcp-socket-connection-refused-for-localhost

Server::Server(unsigned int port) {
    this->cmdList = new CmdListCall();
    this->clients = std::map<int, int>();
    this->clientsThreads = std::map<int, pthread_t>();
    this->cmd = new Commands(false);
    this->retValues = std::map<int, ReturnMessage*>();
    this->running = false;
    this->run = pthread_t();
    struct sockaddr_in socketAdr;

    this->serverID = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); // nastavenia pripojenia

    bzero((char *)&socketAdr, sizeof(socketAdr));
    socketAdr.sin_family = AF_INET;socketAdr.sin_family = AF_INET;
    socketAdr.sin_addr.s_addr = INADDR_ANY;
    socketAdr.sin_port = htons(port);

    if(bind(this->serverID, (struct sockaddr *)&socketAdr, sizeof(socketAdr)) == -1) {
        throw std::logic_error("Server::Server - nepodaril sa bind");
    }

    if(listen(this->serverID, 5) == -1) {
        throw std::logic_error("Server::Server - nepodaril sa listen");
    }
    this->start();
}

Server::~Server() {
    delete this->cmdList;
    this->stop();
    delete this->cmd;
}

void Server::sendMessage(const int client, std::string message) {
    //message.append(" *");
    send(client , message.c_str() , message.length() , 0 );
}

void Server::sendMessageToAll(std::string message) {
    //message.append(" *");
    for (auto cl : this->clients) {
        send(cl.second , message.c_str() , message.length() , 0 );
    }
}

int Server::getServerID() const {
    return this->serverID;
}

bool Server::isRunning() const {
    return this->running;
}

extern "C" void* server_run(void * obj) {
    Server* server = reinterpret_cast<Server*>(obj);
    struct sockaddr_in clientAdr;
    while(server->isRunning()) {
        int size = sizeof(clientAdr);
        int client = accept(server->getServerID(), (struct sockaddr *)&clientAdr, (socklen_t*)(&size));
        if (client >= 0) {
            server->addClient(client);
        }
    }
}

void Server::start() {
    this->running = true;
    pthread_create(&this->run, NULL, &server_run, this);
}

void Server::stop() {
    this->running = false;
    cmdList->exitServer(this);
    for (auto client : this->clients) {
        int cl = client.second;
        shutdown(cl, SHUT_RD);
        close(cl);
        pthread_join(this->clientsThreads[cl], NULL);
        this->removeClient(cl);
        //delete this->clientsThreads[i];
    }
    for (auto thr : this->clientsThreads) {
        int cl = thr.first;
        shutdown(cl, SHUT_RD);
        close(cl);
        pthread_join(thr.second, NULL);
        std::cout<<"Volake spiny tu ostali...\n";
    }
    // thread stale pocuval na accept
    shutdown(this->serverID, SHUT_RD); // https://stackoverflow.com/questions/35754754/how-to-terminate-a-socket-accept-blocking
    close(this->serverID);
    pthread_join(this->run, NULL);
}

typedef struct SCPack {
    Server* server;
    int client;
} SC;

extern "C" void* client_run(void * obj) {
    SC* sc = reinterpret_cast<SC*>(obj);
    char buffer[1024] = {0};
    bzero(buffer, 1024);
    std::cout<<sc->client<<"\n";
    while (read(sc->client , buffer, 1024) > 0 && sc->server->isRunning()) { // 0 - end of file - asi koniec pripojenia
        std::string msg = buffer;
        if (sc->server->addReadedMessage(sc->client, msg)) {
            sc->server->removeClient(sc->client);
            break;
        }
        bzero(buffer, 1024);
        //buffer[1024] = {0};
    }
    delete sc;
}

void Server::addClient(int client) {
    if (!this->running) {
        return;
    }
    this->clients.insert({client, client});
    this->retValues.insert(std::pair<int, ReturnMessage*>(client, new ReturnMessage()));
    SC* sc = new SC();
    sc->server = this;
    sc->client = client;
    pthread_t clientThread;
    this->clientsThreads.insert({client, clientThread});
    pthread_create(&this->clientsThreads[client], NULL, &client_run, sc); // TU MEMORY
    Game::getGame()->addPlayer(client);
}

void Server::removeClient(int client) {
    auto cl = this->clients.find(client);
    this->clients.erase(cl);
    /*auto thr = this->clientsThreads.find(client);
    if (thr != this->clientsThreads.end()) {
        delete thr->second;
        this->clientsThreads.erase(thr);
    }*/
    auto values = this->retValues.find(client);
    if (values != this->retValues.end()) {
        delete values->second;
        this->retValues.erase(values);
    }
    Game::getGame()->removePlayer(client);
}

std::vector<std::string> Server::sendReturnMessage(int client, std::string message) {
    this->sendMessage(client, message);
    std::string prefix = message.substr(0, message.find(' '));
    std::string ret;
    ReturnMessage* values = this->retValues[client];
    do {
        ret = values->getMessage(prefix);
    } while(ret.length() == 0);
    //std::cout<<ret<<"\n";
    std::stringstream output(ret);
    std::vector<std::string> out;
    std::string word;
    while (output >> word) {
        out.emplace_back(word);
    }
    return out;
}

bool Server::addReadedMessage(int client, std::string message) {
    int pos = message.find(' ');
    std::string prefix = message.substr(0, pos);
    if (prefix == "exit") {
        return true;
    }
    std::string param = message.substr(pos + 1, message.length());
    if (prefix == "return") {
        pos = param.find(' ');
        prefix = param.substr(0, pos);
        param = param.substr(pos + 1, param.length());
        this->retValues[client]->addMessage(prefix, param);
    } else {
        this->cmd->executeServer(prefix, this, client, param);
    }
    return false;
}
