//
// Created by jozef on 3. 1. 2021.
//

#include "CmdList.h"
#include "../Game.h"
#include "Client.h"
#include "Server.h"
#include "../Menus/MenuConnection.h"
#include "../Map/GameManager.h"

void CmdList::menuConPlayers(Client *client, std::vector <std::string> params) {
    auto* mn = dynamic_cast<MenuConnection*>(Game::getGame()->getMenu());
    if (mn != nullptr) {
        params.insert(params.begin() + 2, "Prechadzajuce menu");
        params[5] = "Spustit hru";
        params.erase(params.begin() + 6, params.end());
        mn->replace(params);
        mn->show();
    }
}

void CmdList::getData(Client *client, std::vector <std::string> params) {
    Hrac* pl = Game::getGame()->getPlayer();
    std::string ret = "return ";
    ret.append("getData ");
    ret.append(pl->getName());
    ret.append(" ");
    ret.append(std::to_string(pl->getHP()));
    client->sendMessage(ret);
}

void CmdList::runGame(Client *client, std::vector<std::string> params) {
    Game::getGame()->createGame();
}

void CmdList::sendInfoAboutGame(Client *client, std::vector<std::string> params) {
    if (!Game::getGame()->isServerOn()) {
        GameManager::getManager()->infoFromServer(params);
    }
}

void CmdList::sendInfoShot(Server* server, int client, std::vector<std::string> info) {
    GameManager* gm = GameManager::getManager();
    gm->vytvorStreluPos(stoi(info[0]), stoi(info[1]), client, (Direction)stoi(info[2]));
}

void CmdList::sendInfoPlayer(Server* server, int client, std::vector<std::string> info) {
    Game::getGame()->setPlayerData(client, stoi(info[0]), stoi(info[1]), -5);
}

void CmdList::sendInfoUseFirstAid(Server* server, int client, std::vector<std::string> info) {
    Game::getGame()->addHp(client, 1);
    GameManager* gm = GameManager::getManager();
    gm->zmazItemPos(stoi(info[0]),stoi(info[1]));
    gm->vytvorFirstAid();
}

void CmdList::clientId(Client *client, std::vector<std::string> params) {
    Game::getGame()->setMyConnection(stoi(params[0]));
}

void CmdList::stopGame(Server *server, int client, std::vector<std::string> info) {
    std::string str = "endGame"; // getfinalstat uz dava medzeru
    str.append(Game::getGame()->getFinalStat());
    server->sendMessageToAll(str);
}

void CmdList::endGame(Client *client, std::vector<std::string> params) {
    for(auto item : params)
        std::cout << item << "\n";
    for (int i = params.size(); i < 6; ++i) {
        params.emplace_back("...");
    }
    params.insert(params.begin() + 2, " ");
    params.erase(params.begin() + 6, params.end());
    params[5] = "Hlavne menu";
    Game::getGame()->destroyGame();
    Game::getGame()->getMenu()->replace(params);
    Game::getGame()->getMenu()->show();
}
