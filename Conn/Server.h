//
// Created by jozef on 25. 12. 2020.
//

#ifndef SEMESTRALNAPRACA_SERVER_H
#define SEMESTRALNAPRACA_SERVER_H

#include <string>
#include <vector>
#include <pthread.h>
#include "../Game.h"
#include "ReturnMessage.h"
#include "Commands.h"
#include "CmdListCall.h"

class Server {
public:
    Server(unsigned int port);
    ~Server();
    void sendMessage(int client, std::string message);// todo tu bolo const
    std::vector<std::string> sendReturnMessage(int client, std::string message);
    void sendMessageToAll(std::string message);
    int getServerID() const;
    bool isRunning() const;
    void stop();
    void addClient(int client);
    void removeClient(int client);
    bool addReadedMessage(int client, std::string message);
private:
    int serverID;
    std::map<int, int> clients;
    std::map<int, pthread_t> clientsThreads;
    bool running;
    pthread_t run;
    void start();
    std::map<int, ReturnMessage*> retValues;
    Commands* cmd;
    CmdListCall* cmdList;
};


#endif //SEMESTRALNAPRACA_SERVER_H
