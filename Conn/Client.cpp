//
// Created by peter on 25. 12. 2020.
//

#include "Client.h"
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include "ReturnMessage.h"
#include "../Game.h"
#include <bits/stdc++.h>
#include "CmdListCall.h"
#include <fcntl.h>
#include "../Menus/MenuStart.h"
#include "../Menus/MenuConnection.h"

Client::Client(std::string server,unsigned int port) {
    this->cmdCall = new CmdListCall();
    this->cmd = new Commands(true);
    struct hostent *adresaServeru;
    struct sockaddr_in config;
    //unsigned char byte;+

    adresaServeru = gethostbyname(server.c_str());
    bzero((char *)&config, sizeof(config));
    config.sin_family = AF_INET;
    memcpy(&(config.sin_addr),  adresaServeru->h_addr, adresaServeru->h_length);
    config.sin_port = htons(port);
    conId = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    //fcntl(conId, F_SETFL, O_NONBLOCK); // neblokuje socket

    if (connect(conId, (struct sockaddr *) &config, sizeof(config)) == -1) {
        fprintf(stderr, "connect: %s\n", strerror(errno));
        //exit(0);
        this->reading = false;
    } else {
        startReading();
    }
    this->retValues = new ReturnMessage();
}

Client::~Client() {
    //cmdCall->exitClient(this);
    stopReading();
    //close(conId);
    pthread_join(this->p_reading, NULL);
    delete this->cmd;
    delete this->cmdCall;
    delete this->retValues;
}

void Client::sendMessage(std::string message) {
    //message.append(" *");
    send(conId, message.c_str(), message.length(), 0);
}

extern "C" void* runReadingMessage(void * obj) {
    Client* client = reinterpret_cast<Client*>(obj);
    char buffer[1024] = {0};
    bzero(buffer, 1024);
    while (client->isRunningReading() && read(client->getConId(), buffer, 1024) > 0)
    {
        std::string msg = buffer;
        if (client->addReadedMessage(msg)) {
            Game::getGame()->stopConnect(false);
            //client->stopReading();
            break;
        }
        bzero(buffer, 1024);
        //buffer[1024] = {0};
    }
}

void Client::startReading() {
    this->reading = true;
    pthread_create( &this->p_reading, NULL, &runReadingMessage, this);
}

void Client::stopReading() {
    if (this->reading) {
        this->reading = false;
        cmdCall->exitClient(this);
        shutdown(this->conId, SHUT_RD);
        close(this->conId);
        if (!Game::getGame()->isServerOn()) {
            //Game::getGame()->setMenu(new MenuStart());
            auto* mn = dynamic_cast<MenuConnection*>(Game::getGame()->getMenu());
            if (mn != nullptr) {
                mn->setMessage("* !!! Server bol ukonceny! Prosim odpojte sa... !!! *");
                mn->show();
            }
        }
        //Game::getGame()->setMenu(new MenuStart());
        //pthread_join(this->p_reading, NULL);
    }
}

bool Client::isRunningReading() {
    return this->reading;
}

int Client::getConId() const {
    return conId;
}

std::vector<std::string> Client::sendReturnMessage(std::string message) {
    this->sendMessage(message);
    std::string prefix = message.substr(0, message.find(' '));
    std::string ret;
    do {
        ret = this->retValues->getMessage(prefix);
    } while(ret.length() == 0);
    std::stringstream output(ret);
    std::vector<std::string> out;
    std::string word;
    while (output >> word) {
        out.emplace_back(word);
    }
    return out;
}

bool Client::addReadedMessage(std::string message) {
    int pos = message.find(' ');
    std::string prefix = message.substr(0, pos);
    if (prefix == "exit") {
        return true;
    }
    std::string param = message.substr(pos + 1, message.length());
    if (prefix == "return") {
        pos = param.find(' ');
        prefix = param.substr(0, pos);
        param = param.substr(pos + 1, param.length());
        this->retValues->addMessage(prefix, param);
    } else {
        this->cmd->executeClient(prefix, this, param);
    }
    return false;
}
