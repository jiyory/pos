//
// Created by jozef on 3. 1. 2021.
//

#ifndef SEMESTRALNAPRACA_CMDLISTCALL_H
#define SEMESTRALNAPRACA_CMDLISTCALL_H

class Server;
class Client;
#include <vector>
#include <string>

class CmdListCall {
public:
    /* SERVER */
    void sendInfoAboutPlayers(Server* server, std::vector<std::string> players);
    void sendInfoAboutGame(Server* server, std::string players, std::string map);
    void exitServer(Server* server);
    void runGame(Server* server);
    void clientId(Server* server, int id);
    /* CLIENT */
    void exitClient(Client* client);
    void sendInforUseFirstAid(Client* client, std::string position);
    void sendInfoShot(Client* client, std::string info);
    void sendInfoPlayer(Client* client, std::string position);
    void stopGame(Client* client);
};


#endif //SEMESTRALNAPRACA_CMDLISTCALL_H
