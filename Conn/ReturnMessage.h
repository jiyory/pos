//
// Created by jozef on 2. 1. 2021.
//

#ifndef SEMESTRALNAPRACA_RETURNMESSAGE_H
#define SEMESTRALNAPRACA_RETURNMESSAGE_H

#include <map>

class ReturnMessage {
public:
    ReturnMessage();
    ~ReturnMessage();
    bool addMessage(std::string prefix, std::string param);
    std::string getMessage(std::string &prefix);
private:
    std::map<std::string, std::string> retValues;
    pthread_mutex_t mutex;
};


#endif //SEMESTRALNAPRACA_RETURNMESSAGE_H
