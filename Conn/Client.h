//
// Created by peter on 25. 12. 2020.
//

#ifndef SEMESTRALNAPRACA_CLIENT_H
#define SEMESTRALNAPRACA_CLIENT_H


#include <string>
#include "ReturnMessage.h"
#include <vector>
#include "Commands.h"
#include "CmdListCall.h"

class Client {
public:
    Client(std::string server, unsigned int port);
    void sendMessage(std::string message);
    std::vector<std::string> sendReturnMessage(std::string message);
    virtual ~Client();
    bool isRunningReading();
    void startReading();
    int getConId() const;
    void stopReading();
    bool addReadedMessage(std::string message);
private:
    int conId;
    bool reading;
    pthread_t p_reading{};
    ReturnMessage* retValues;
    Commands* cmd;
    CmdListCall* cmdCall;
};


#endif //SEMESTRALNAPRACA_CLIENT_H
