//
// Created by jozef on 3. 1. 2021.
//

#include "Commands.h"
#include "CmdList.h"
#include <map>
#include <bits/stdc++.h>

Commands::Commands(bool client) {
    this->cmdList = new CmdList();
    if (client) {
        this->functionC = std::map<std::string, void (CmdList::*)(Client*, std::vector<std::string>)>();

        this->functionC.insert(std::pair<std::string, void (CmdList::*)(Client*, std::vector<std::string>)>(
                "menuConPlayers", &CmdList::menuConPlayers));
        this->functionC.insert(std::pair<std::string, void (CmdList::*)(Client*, std::vector<std::string>)>(
                "getData", &CmdList::getData));
        this->functionC.insert(std::pair<std::string, void (CmdList::*)(Client*, std::vector<std::string>)>(
                "runGame", &CmdList::runGame));
        this->functionC.insert(std::pair<std::string, void (CmdList::*)(Client*, std::vector<std::string>)>(
                "sendInfoAboutGame", &CmdList::sendInfoAboutGame));
        this->functionC.insert(std::pair<std::string, void (CmdList::*)(Client*, std::vector<std::string>)>(
                "clientId", &CmdList::clientId));
        this->functionC.insert(std::pair<std::string, void (CmdList::*)(Client*, std::vector<std::string>)>(
                "endGame", &CmdList::endGame));
    } else {
        this->functionS = std::map<std::string, void (CmdList::*)(Server*, int, std::vector<std::string>)>();
        this->functionS.insert(std::pair<std::string, void (CmdList::*)(Server*, int, std::vector<std::string>)>(
                "sendInfoShot", &CmdList::sendInfoShot));
        this->functionS.insert(std::pair<std::string, void (CmdList::*)(Server*, int, std::vector<std::string>)>(
                "sendInfoPlayer", &CmdList::sendInfoPlayer));
        this->functionS.insert(std::pair<std::string, void (CmdList::*)(Server*, int, std::vector<std::string>)>(
                "sendInforUseFirstAid", &CmdList::sendInfoUseFirstAid));
        this->functionS.insert(std::pair<std::string, void (CmdList::*)(Server*, int, std::vector<std::string>)>(
                "stopGame", &CmdList::stopGame));
    }
}

Commands::~Commands() {
    delete this->cmdList;
}

void Commands::executeServer(std::string &cmd, Server *server, int client, std::string param) {
    auto ex = this->functionS.find(cmd);
    if (ex != this->functionS.end()) {
        std::vector<std::string> params;
        std::string par;
        int i = param.find(' ');
        while (i != std::string::npos) {
            par = param.substr(0, i);
            param.erase(0, i+1);
            params.emplace_back(par);
            i = param.find(' ');
        }
        if (param.length() > 0)
            params.emplace_back(param);
        void (CmdList::*fun)(Server*, int, std::vector<std::string>) = ex->second;
        (this->cmdList->*fun)(server, client, params);
    } else {
        std::cout<<"Neexistujuci prefix: "<<cmd<<"\n";
    }
}

void Commands::executeClient(std::string &cmd, Client *client, std::string param) {
    auto ex = this->functionC.find(cmd);
    if (ex != this->functionC.end()) {
        std::vector<std::string> params;
        std::string par;
        int i = param.find(' ');
        while (i != std::string::npos) {
            par = param.substr(0, i);
            param.erase(0, i+1);
            params.emplace_back(par);
            i = param.find(' ');
        }
        if (param.length() > 0)
            params.emplace_back(param);

        void (CmdList::*fun)(Client*, std::vector<std::string>) = ex->second;
        (this->cmdList->*fun)(client, params);
    } else {
        std::cout<<"Neexistujuci prefix: "<<cmd<<"\n";
    }
}
